namespace Vixa.StickEating
{
    using Fever;
    using Zenject;

    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<GameController>().AsSingle().NonLazy();

            Container.BindInterfacesAndSelfTo<PointsController>().AsSingle().NonLazy();

            Container.BindInterfacesAndSelfTo<FeverController>().AsSingle().NonLazy();
            
            DeclareSignals();
        }

        private void DeclareSignals()
        {
            Container.DeclareSignal<GameStateChangedSignal>().OptionalSubscriber();
            
            Container.DeclareSignal<MonsterDeadSignal>().OptionalSubscriber();
            Container.DeclareSignal<MonsterStartEatingSignal>();
            Container.DeclareSignal<MonsterFullSignal>();
            
            Container.DeclareSignal<StickCompletedSignal>();
        }
    }
}
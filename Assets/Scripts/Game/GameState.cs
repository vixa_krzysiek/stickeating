namespace Vixa.StickEating
{
    public enum GameState
    {
        Start,
        Playing,
        GameWon,
        GameLost
    }
}
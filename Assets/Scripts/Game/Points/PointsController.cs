namespace Vixa.StickEating
{
    using System;
    using Zenject;

    public class PointsController : IInitializable
    {
        private const string BEST_POINTS_SAVE_KEY = "best_points";
        
        private readonly IStorage _storage;
        private int _amount;
        private int _bestPoints;
        
        public event Action OnPointsUpdated;
        public event Action OnNewBestReached;
        public int Amount
        {
            get => _amount;
            private set
            {
                _amount = value;
                OnPointsUpdated?.Invoke();
                CheckForBest();
            }
        }
        public int BestPoints
        {
            get => _bestPoints;
            private set
            {
                _bestPoints = value;
                OnNewBestReached?.Invoke();
            }
        }
        
        public PointsController(
            IStorage storage)
        {
            _storage = storage;
        }

        public void Initialize()
        {
            _bestPoints = _storage.LoadInt(BEST_POINTS_SAVE_KEY);
        }

        public void AddPoints(int amount)
        {
            if(amount <= 0) { return; }
            Amount += amount;
        }

        private void CheckForBest()
        {
            if (Amount > BestPoints)
            {
                BestPoints = Amount;
            }
        }
    }
}
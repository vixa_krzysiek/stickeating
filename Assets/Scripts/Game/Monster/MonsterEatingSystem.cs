namespace Vixa.StickEating
{
    using UnityEngine;
    using Zenject;

    public class MonsterEatingSystem : ITickable
    { 
        private readonly MonsterAnimationsHandler _monsterAnimations;
        private readonly MonsterBellyHandler _monsterBelly;
        private readonly MonsterModel _monsterModel;
        private readonly Stick _stick;
        private readonly SignalBus _signalBus;

        private bool _wasEatingAlready;
        
        public MonsterEatingSystem(
            MonsterAnimationsHandler monsterAnimations,
            MonsterBellyHandler monsterBelly,
            MonsterModel monsterModel,
            Stick stick,
            SignalBus signalBus)
        {
            _monsterAnimations = monsterAnimations;
            _monsterBelly = monsterBelly;
            _monsterModel = monsterModel;
            _stick = stick;
            _signalBus = signalBus;
            
            signalBus.Subscribe<TouchSignal>(CheckInput);
            _monsterModel.IsWorking = true;
        }

        public void Tick()
        {
            if(!_monsterModel.IsWorking) { return; }
            ReactOnStick();
        }

        private void ReactOnStick()
        {
            if (_monsterModel.IsEating)
            {
                if (!_wasEatingAlready)
                {
                    _wasEatingAlready = true;
                    _signalBus.Fire<MonsterStartEatingSignal>();
                }
                _monsterAnimations.DoEatAnimation();
            }
            else
            {
                _monsterAnimations.DoNotEating();
            }
        
            if (_stick.IsMoving)
            {
                return;
            }
            
            if (_monsterModel.IsEating)
            {
                var part = _stick.EatPart();
                _monsterBelly.ConsumePart(part);
            }
            else
            {
                _stick.DestroyPart();
            }
        }
        
        private void CheckInput(TouchSignal args)
        {
            if (!_monsterModel.IsEating && args.TouchPhase == TouchPhase.Began)
            {
                _monsterModel.IsEating = true;
            }
            else if (_monsterModel.IsEating &&
                     (args.TouchPhase == TouchPhase.Ended || args.TouchPhase == TouchPhase.Canceled))
            {
                _monsterModel.IsEating = false;
            }
        }
    }
}
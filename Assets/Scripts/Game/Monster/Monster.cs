﻿namespace Vixa.StickEating
{
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using Zenject;

    public class Monster : MonoBehaviour
    {
        [Inject] private readonly MonsterModel _monsterModel = null;
        [Inject] private readonly MonsterBellyHandler _monsterBellyHandler = null;

        public bool IsEating => _monsterModel.IsEating;
        public bool IsBellyFull => _monsterBellyHandler.IsFull;
        public int BellyNeededAmount => _monsterBellyHandler.NeededAmount;
        public int BellyCurrentAmount => _monsterBellyHandler.ConsumedAmount;

        public bool IsImmortal
        {
            get => _monsterModel.IsImmortal;
            set => _monsterModel.IsImmortal = value;
        }
       
    }
}


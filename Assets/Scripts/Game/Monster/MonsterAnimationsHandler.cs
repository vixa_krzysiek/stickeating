namespace Vixa.StickEating
{
    using DG.Tweening;
    using UnityEngine;

    public class MonsterAnimationsHandler
    {
        private readonly MonsterView _monsterView;
        
        private Tweener _eatTweener;
        private bool _isEatingAnimation;

        public MonsterAnimationsHandler(MonsterView monsterView)
        {
            _monsterView = monsterView;
        }
        
        public void DoEatAnimation()
        {
            if(_isEatingAnimation) { return; }
            
            _isEatingAnimation = true;
            _eatTweener = _monsterView.Mouth
                .DOScaleZ(1.4f, 0.1f)
                .OnComplete(() =>
                {
                    _monsterView.Mouth
                        .DOScaleZ(0.4f, .1f)
                        .OnComplete(() => _isEatingAnimation = false);
                });

            Camera.main.DOShakeRotation(0.3f,  new Vector3(0f, .5f, .5f), 2); //TODO: move camera from here
        }

        public void DoNotEating()
        {
            var scale = _monsterView.Mouth.localScale;
            scale.z = 0.1f;
            _monsterView.Mouth.localScale = scale;
            Camera.main.transform.DORotate(new Vector3(20, 0,0),  0.4f); //TODO: move camera from here
        }
    }
}
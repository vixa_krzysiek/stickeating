namespace Vixa.StickEating
{
    using Zenject;

    public class MonsterInstaller : MonoInstaller<MonsterInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<MonsterView>().FromInstance(GetComponent<MonsterView>()).AsSingle();
            
            Container.BindInterfacesAndSelfTo<MonsterModel>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<MonsterEatingSystem>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<MonsterBellyHandler>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<MonsterAnimationsHandler>().AsSingle();
        }
    }
}
namespace Vixa.StickEating
{
    public class MonsterModel
    {
        public bool IsWorking { get; set; }
        public bool IsEating { get; set; }
        public bool IsImmortal { get; set; }
    }
}
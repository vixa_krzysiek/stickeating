namespace Vixa.StickEating
{
    using Zenject;

    public class MonsterBellyHandler
    {
        private readonly SignalBus _signalBus;
        private readonly MonsterModel _monsterModel;
        private readonly PointsController _pointsController;
        
        private int _neededAmount;
        private int _consumedAmount;
        private bool _isFull;
        
        public int NeededAmount
        {
            get => _neededAmount;
            private set => _neededAmount = value;
        }

        public int ConsumedAmount
        {
            get => _consumedAmount;
            private set
            {
                if(IsFull) { return; }
                
                _consumedAmount = value;
                if (_consumedAmount >= _neededAmount)
                {
                    IsFull = true;
                }
            }
        }

        public bool IsFull
        {
            get => _isFull;
            private set
            {
                _isFull = value;
                if (value)
                {
                    _signalBus.Fire<MonsterFullSignal>();
                }
            }
        }

        public MonsterBellyHandler(
            SignalBus signalBus, 
            MonsterModel monsterModel,
            PointsController pointsController)
        {
            _signalBus = signalBus;
            _monsterModel = monsterModel;
            _pointsController = pointsController;
            _neededAmount = 50;
        }
        
        public void ConsumePart(StickPart stickPart) //TODO: do it better (remove if else)
        {
            if(stickPart == null) { return; }
            if (stickPart.Id == StickPartId.Regular)
            {
                ConsumedAmount += 1;
                _pointsController.AddPoints(10);   
            }
            else if(!_monsterModel.IsImmortal)
            {
                _monsterModel.IsWorking = false;
                _signalBus.Fire<MonsterDeadSignal>();
            }
        }
    }
}
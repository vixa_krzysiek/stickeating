namespace Vixa.StickEating
{
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using Zenject;

    public class GameController : IInitializable
    {
        private const string LEVEL_NUMBER_SAVE_KEY = "level_number";

        private readonly SignalBus _signalBus = null;
        private readonly IStorage _storage = null;
        
        private GameState _gameState;

        public int LevelNumber { get; private set; }
        public GameState GameState
        {
            get => _gameState;
            private set
            {
                _gameState = value;
                _signalBus.Fire(new GameStateChangedSignal{State = _gameState});
            }
        }

        public GameController(
            IStorage storage,
            SignalBus signalBus)
        {
            _signalBus = signalBus;
            _storage = storage;
            
            _signalBus.Subscribe<MonsterStartEatingSignal>(StartGame);
            _signalBus.Subscribe<MonsterDeadSignal>(GameLost);
            _signalBus.Subscribe<StickCompletedSignal>(GameWin);
        }
        
        public void Initialize()
        {
            LevelNumber = _storage.LoadInt(LEVEL_NUMBER_SAVE_KEY);
            Debug.Log(LevelNumber);

            GameState = GameState.Start;
        }

        public void PlayNext()
        {
            LevelNumber += 1;
            _storage.Save(LEVEL_NUMBER_SAVE_KEY, LevelNumber);
            
            ReloadGame();
        }

        public void Restart()
        {
            ReloadGame();
        }

        private void StartGame()
        {
            GameState = GameState.Playing;
        }
        
        private void GameLost()
        {
            GameState = GameState.GameLost;
        }

        private void GameWin()
        {
            GameState = GameState.GameWon;
        }

        private void ReloadGame()
        {
            SceneManager.LoadScene(0);
        }
    }
}
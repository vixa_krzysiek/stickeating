namespace Vixa.StickEating
{
    using UnityEngine;
    using Zenject;

    public class StickController : IInitializable
    {
        private readonly StickQueue _stickQueue;
        private readonly StickPartsFactory _partsFactory;
        private readonly Monster _monster;
        private readonly SignalBus _signalBus;
            
        private int _onStickIndex;
        private bool _lastSequenceCreated;

        public StickController(
            StickQueue stickQueue,
            StickPartsFactory partsFactory,
            Monster monster,
            SignalBus signalBus)
        {
            _stickQueue = stickQueue;
            _partsFactory = partsFactory;
            _monster = monster;
            _signalBus = signalBus;
        }
        
        public void Initialize()
        {
            CreateNewSequence();
            CreateNewSequence();
        }

        public StickPart EatPart()
        {
            if(_stickQueue.Count <= 0) { return null; }
            var part = _stickQueue.RemovePart();
            part.Eat();
            CheckIfNextSequenceNeeded(part.OnStickIndex);
            CheckIfStickDone();
            return part;
        }

        public void DestroyPart()
        {
            if(_stickQueue.Count <= 0) { return; }
            var part = _stickQueue.RemovePart();
            part.Destroy();
            CheckIfNextSequenceNeeded(part.OnStickIndex);
            CheckIfStickDone();
        }

        private void CheckIfNextSequenceNeeded(int partIndex)
        {
            if (_monster.IsBellyFull)
            {
                if (!_lastSequenceCreated)
                {
                    _lastSequenceCreated = true;
                    CreateLastSequence();
                }

                return;
            }
            var difference = _onStickIndex - partIndex;
            if (difference < 10)
            {
                CreateNewSequence();
            }
        }

        private void CheckIfStickDone()
        {
            if (_stickQueue.Count <= 0)
            {
                _signalBus.Fire<StickCompletedSignal>();
            }
        }
        
        private void CreateLastSequence()
        {
            var length = Random.Range(6, 12);
            for (var k = 0; k < length; k++)
            {
                var newPart = _partsFactory.CreatePart(StickPartId.Regular, _onStickIndex++);
                newPart.transform.localPosition = new Vector3(0, _onStickIndex, 0);
            }
        }
        
        private void CreateNewSequence()
        {
            var length = Random.Range(4, 12);
            for (var k = 0; k < length; k++)
            {
                var newPart = _partsFactory.CreatePart(StickPartId.Regular, _onStickIndex++);
                newPart.transform.localPosition = new Vector3(0, _onStickIndex, 0);
            }
                
            length = Random.Range(1, 5);
            for (var k = 0; k < length; k++)
            {
                var newPart = _partsFactory.CreatePart(StickPartId.Deadly, _onStickIndex++);
                newPart.transform.localPosition = new Vector3(0, _onStickIndex, 0);
            }
        }
    }
}
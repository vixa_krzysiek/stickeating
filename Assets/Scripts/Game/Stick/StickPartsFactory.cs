namespace Vixa.StickEating
{
    using System;

    public class StickPartsFactory
    {
        private readonly StickQueue _stickQueue;
        private readonly RegularStickPart.Pool _regularStickPartPool;
        private readonly DeadlyStickPart.Pool _deadlyStickPartPool;
        
        public StickPartsFactory(
            StickQueue stickQueue,
            RegularStickPart.Pool regularStickPartPool,
            DeadlyStickPart.Pool deadlyStickPartPool)
        {
            _stickQueue = stickQueue;
            _regularStickPartPool = regularStickPartPool;
            _deadlyStickPartPool = deadlyStickPartPool;
        }

        public StickPart CreatePart(StickPartId id, int index)
        {
            StickPart newPart;
            switch (id)
            {
                case StickPartId.Regular:
                    newPart = _regularStickPartPool.Spawn();
                    break;
                case StickPartId.Deadly:
                    newPart = _deadlyStickPartPool.Spawn();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(id), id, null);
            }

            newPart.OnStickIndex = index;
            _stickQueue.AddPart(newPart);
            return newPart;
        }
    }
}
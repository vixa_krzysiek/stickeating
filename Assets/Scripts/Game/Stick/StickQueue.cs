namespace Vixa.StickEating
{
    using System.Collections.Generic;
    using System.Linq;

    public class StickQueue
    {
        private readonly Queue<StickPart> _stickParts;

        public Queue<StickPart> AllStickParts => _stickParts;
        
        public int Count => _stickParts.Count;
        public StickPart LastElement => _stickParts.Last();

        public StickQueue()
        {
            _stickParts = new Queue<StickPart>(10);
        }
        
        public void AddPart(StickPart part)
        {
            _stickParts.Enqueue(part);
        }

        public StickPart RemovePart()
        {
            return _stickParts.Dequeue();
            
            
        }
    }
}
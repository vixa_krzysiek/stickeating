﻿namespace Vixa.StickEating
{
    using Zenject;

    public class DeadlyStickPart : StickPart
    {
        public override StickPartId Id => StickPartId.Deadly;
        
        public class Pool : MemoryPool<DeadlyStickPart>
        {
            protected override void OnCreated(DeadlyStickPart item)
            {
                item.gameObject.SetActive(false);
            }

            protected override void OnSpawned(DeadlyStickPart item)
            {
                item.gameObject.SetActive(true);
            }
        }
    }
}




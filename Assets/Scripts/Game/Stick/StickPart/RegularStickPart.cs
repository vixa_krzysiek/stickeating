namespace Vixa.StickEating
{
    using Zenject;

    public class RegularStickPart : StickPart
    {
        public override StickPartId Id => StickPartId.Regular;
        
        public class Pool : MemoryPool<RegularStickPart>
        {
            protected override void OnCreated(RegularStickPart item)
            {
                item.gameObject.SetActive(false);
            }

            protected override void OnSpawned(RegularStickPart item)
            {
                item.gameObject.SetActive(true);
            }
        }
    }
}
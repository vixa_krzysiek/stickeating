﻿namespace Vixa.StickEating
{
    using UnityEngine;

    public abstract class StickPart : MonoBehaviour
    {
        [SerializeField] private GameObject _destroyEffect = null;

        public int OnStickIndex { get; set; }

        public abstract StickPartId Id { get; }
        
        public void Eat()
        {
            if (_destroyEffect != null)
            {
                var effect = Instantiate(_destroyEffect);
                effect.SetActive(true);
                Destroy(effect, 2f);
            }
            
            Destroy(gameObject);
        }

        public void Destroy()
        {
            var rigidbody = GetComponent<Rigidbody>();
            rigidbody.isKinematic = false;
            var randomSign = Random.Range(0, 2) < 1 ? 1 : -1;
            var yStrength = Random.Range(50, 250);
            var xStrength = Random.Range(250, 500);
            rigidbody.AddForce(new Vector3(randomSign*xStrength,yStrength,0));

            //transform.DOMove(new Vector3(5, 0, 0), 0.5f).OnComplete(() => Destroy(gameObject));

        }

    }
}



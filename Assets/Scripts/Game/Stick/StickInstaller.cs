namespace Vixa.StickEating
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class StickInstaller : MonoInstaller<StickInstaller>
    {
        [SerializeField] private StickPartsCatalog _partsCatalog = null;
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<StickController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<StickQueue>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<StickPartsCatalog>().FromInstance(_partsCatalog).AsSingle();

            Container.BindInterfacesAndSelfTo<StickPartsFactory>().AsSingle();
            Container.BindMemoryPool<RegularStickPart, RegularStickPart.Pool>()
                .WithInitialSize(10)
                .FromComponentInNewPrefab(_partsCatalog.RegularStickPartPrefab);
            Container.BindMemoryPool<DeadlyStickPart, DeadlyStickPart.Pool>()
                .WithInitialSize(10)
                .FromComponentInNewPrefab(_partsCatalog.DeadlyStickPartPrefab);
        }
    }

    [Serializable]
    public class StickPartsCatalog
    {
        public RegularStickPart RegularStickPartPrefab;
        public DeadlyStickPart DeadlyStickPartPrefab;
    }
}
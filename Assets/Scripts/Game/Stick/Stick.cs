﻿namespace Vixa.StickEating
{
    using DG.Tweening;
    using UnityEngine;
    using Zenject;

    public class Stick : MonoBehaviour
    {
        [Inject] private readonly StickController _stickController = null;
        [Inject] private readonly Monster _monster = null;

        public bool IsMoving { get; private set; }

        public StickPart EatPart()
        {
            var part = _stickController.EatPart();
            MoveDown();
            return part;
        }
        
        public void DestroyPart()
        {
            _stickController.DestroyPart();
            MoveDown();
        }

        public void MoveDown()
        {
            IsMoving = true;
            var newPosition = transform.position;
            newPosition.y -= 1;

            transform.DOMove(newPosition, _monster.IsEating ? 0.15f : 0.35f).SetEase(Ease.Linear).OnComplete(() => IsMoving = false);
        }
    }
}



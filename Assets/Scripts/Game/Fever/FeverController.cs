namespace Vixa.StickEating.Fever
{
    using UnityEngine;
    using Zenject;

    public class FeverController : ITickable
    {
         private const float UP_SPEED = 1f;
        private const float DOWN_SPEED = 0.3f;
        private const float FEVER_DURATION = 2f;
        
        private readonly Monster _monster;

        public bool IsFeverActive { get; private set; }
        public float FeverProgress { get; private set; }
        public float FeverDuration { get; private set; }
        
        public FeverController(Monster monster)
        {
            _monster = monster;
        }
        
        public void Tick()
        {
            if (IsFeverActive)
            {
                UpdateFeverDuration();
            }
            else
            {
                UpdateFeverProgress();
                ActivateFeverIfReady();
            }
            
            Debug.Log($"Progress: {FeverProgress} Duration: {FeverDuration} Active: {IsFeverActive}");
        }

        private void UpdateFeverProgress()
        {
            if (_monster.IsEating)
            {
                FeverProgress += UP_SPEED * Time.deltaTime;
            }
            else
            {
                FeverProgress -= DOWN_SPEED * Time.deltaTime;
            }

            FeverProgress = Mathf.Clamp(FeverProgress, 0, 1);
        }
        
        private void ActivateFeverIfReady()
        {
            if (FeverProgress >= 1 && !IsFeverActive)
            {
                ActivateFever();
            }
        }

        private void ActivateFever()
        {
            IsFeverActive = true;
            _monster.IsImmortal = true;
            FeverDuration = 0f;
        }

        private void DeactivateFever()
        {
            IsFeverActive = false;
            _monster.IsImmortal = false;
            FeverDuration = 0f;
            FeverProgress = 0f;
        }
        
        private void UpdateFeverDuration()
        {
            FeverDuration += Time.deltaTime;
            if (FeverDuration >= FEVER_DURATION)
            {
                DeactivateFever();
            }
        }
    }
}
﻿namespace Vixa
{
    using DG.Tweening;
    using UnityEngine;

    [RequireComponent(typeof(CanvasGroup))]
    public abstract class UiPanel : MonoBehaviour
    {
        private CanvasGroup _canvasGroup;
        protected CanvasGroup CanvasGroup
        {
            get
            {
                if (_canvasGroup == null)
                {
                    _canvasGroup = GetComponent<CanvasGroup>();
                }

                return _canvasGroup;
            }
        }
        
        public virtual void Show(bool ignoreAnimation = false)
        {
            CanvasGroup.blocksRaycasts = true;

            if (ignoreAnimation)
            {
                _canvasGroup.alpha = 1f;
            }
            else
            {
                ShowAnimation();
            }
        }

        public virtual void Hide(bool ignoreAnimation = false)
        {
            CanvasGroup.blocksRaycasts = false;

            if (ignoreAnimation)
            {
                _canvasGroup.alpha = 0f;
            }
            else
            {
                HideAnimation();
            }
        }

        protected virtual void ShowAnimation()
        {
            CanvasGroup.DOFade(1f, 0.5f);
        }

        protected virtual void HideAnimation()
        {
            CanvasGroup.DOFade(0f, 0.5f);
        }
    }
}


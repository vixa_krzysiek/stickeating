﻿namespace Vixa.StickEating
{
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class UiWinScreen : UiPanel
    {
        [Inject] private readonly SignalBus _signalBus = null;
        [Inject] private readonly GameController _gameController = null;
        
        [SerializeField] private Button _playNextButton = null;
        
        private void Start()
        {
            Hide(true);
            
            _signalBus.Subscribe<GameStateChangedSignal>(x =>
            {
                if (x.State == GameState.GameWon)
                {
                    Show();
                }
            });
            
            _playNextButton.onClick.RemoveAllListeners();
            _playNextButton.onClick.AddListener(() => _gameController.PlayNext());
        }
    }
}


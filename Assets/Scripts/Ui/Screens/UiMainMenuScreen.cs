namespace Vixa.StickEating
{
    using Zenject;

    public class UiMainMenuScreen : UiPanel
    {
        [Inject] private readonly SignalBus _signalBus = null;
        
        private void Start()
        {
            _signalBus.Subscribe<GameStateChangedSignal>(x =>
            {
                if (x.State == GameState.Playing)
                {
                    Hide();
                }
            });
        }
    }
}
﻿namespace Vixa.StickEating
{
    using Zenject;

    public class UiIngameScreen : UiPanel
    {
        [Inject] private readonly SignalBus _signalBus = null;
        
        private void Start()
        {
            Hide(true);
            
            _signalBus.Subscribe<GameStateChangedSignal>(x =>
            {
                if (x.State == GameState.Playing)
                {
                    Show();
                }
                else
                {
                    Hide();
                }
            });
        }
    }
}

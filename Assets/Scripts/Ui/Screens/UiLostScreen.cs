﻿namespace Vixa.StickEating
{
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class UiLostScreen : UiPanel
    {
        [Inject] private readonly SignalBus _signalBus = null;
        [Inject] private readonly GameController _gameController = null;
        
        [SerializeField] private Button _restartButton = null;
        
        private void Start()
        {
            Hide(true);
            
            _signalBus.Subscribe<GameStateChangedSignal>(x =>
            {
                if (x.State == GameState.GameLost)
                {
                    Show();
                }
            });
            
            _restartButton.onClick.RemoveAllListeners();
            _restartButton.onClick.AddListener(() => _gameController.Restart());
        }
    }
}


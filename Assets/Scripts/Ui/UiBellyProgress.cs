﻿namespace Vixa.StickEating
{
    using DG.Tweening;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class UiBellyProgress : MonoBehaviour
    {
        [Inject] private readonly Monster _monster = null;
        
        [SerializeField] private Image _progressImage = null;

        private bool _isFull;
        
        private void Update()
        {
            if(_isFull) { return; }
            
            var fillAmount = _monster.BellyCurrentAmount / (float) _monster.BellyNeededAmount;
            _progressImage.fillAmount = fillAmount;
            
            if (fillAmount >= 1)
            {
                _isFull = true;
                _progressImage.DOColor(Color.green, 0.5f);
            }
        }
    }
}
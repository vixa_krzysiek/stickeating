﻿namespace Vixa.StickEating
{
    using TMPro;
    using UnityEngine;
    using Zenject;

    public class UiLevelNumber : MonoBehaviour
    {
        [Inject] private readonly GameController _gameController = null;
        
        [SerializeField] private TextMeshProUGUI _levelText = null;

        private void Start()
        {
            _levelText.text = $"LEVEL {_gameController.LevelNumber + 1}";
        }
    }
}
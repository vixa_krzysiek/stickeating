﻿namespace Vixa.StickEating
{
    using TMPro;
    using UnityEngine;
    using Zenject;

    public class UiPoints : MonoBehaviour
    {
        [Inject] private readonly PointsController _pointsController = null;
        
        [SerializeField] private TextMeshProUGUI _pointsText = null;

        private void Start()
        {
            _pointsController.OnPointsUpdated += UpdatePoints;
            UpdatePoints();
        }

        private void UpdatePoints()
        {
            _pointsText.text = _pointsController.Amount.ToString();
        }
    }
}


﻿namespace Vixa
{
    using Zenject;

    public class InputInstaller : Installer<InputInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<InputUiClicksChecker>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<BaseTouchInput>().AsSingle().NonLazy();

            Container.DeclareSignal<TouchSignal>();
        }
    }
}


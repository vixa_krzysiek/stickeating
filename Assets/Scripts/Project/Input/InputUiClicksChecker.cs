﻿namespace Vixa
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class InputUiClicksChecker
    {
        private readonly EventSystem _eventSystem;
        private readonly PointerEventData _pointerEventData;

        private readonly List<RaycastResult> _raycastResults = new List<RaycastResult>();
        
        public InputUiClicksChecker(EventSystem eventSystem)
        {
            _eventSystem = eventSystem;
            _pointerEventData = new PointerEventData(_eventSystem);
        }
        
        public bool IsClickOverUi(Vector2 clickPosition)
        {
            _pointerEventData.position = clickPosition;
            _eventSystem.RaycastAll(_pointerEventData, _raycastResults);
            return _raycastResults.Count != 0;
        }
    }
}
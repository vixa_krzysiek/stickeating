﻿namespace Vixa
{
    using UnityEngine;

    public class TouchSignal
    {
        public TouchPhase TouchPhase;
        public Vector2 Position;
    }
}


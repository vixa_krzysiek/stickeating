﻿namespace Vixa
{
    using UnityEngine;
    using Zenject;

    public class BaseTouchInput : ITickable
    {
        private readonly SignalBus _signalBus;
        private readonly InputUiClicksChecker _uiClicksChecker;
        
        public BaseTouchInput(SignalBus signalBus, InputUiClicksChecker uiClicksChecker)
        {
            _signalBus = signalBus;
            _uiClicksChecker = uiClicksChecker;
        }
        
        public void Tick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if(_uiClicksChecker.IsClickOverUi(Input.mousePosition)) { return; }
                _signalBus.Fire(new TouchSignal
                {
                    Position = Input.mousePosition, 
                    TouchPhase = TouchPhase.Began
                });
            }
            else if (Input.GetMouseButton(0))
            {
                if(_uiClicksChecker.IsClickOverUi(Input.mousePosition)) { return; }
                _signalBus.Fire(new TouchSignal
                {
                    Position = Input.mousePosition, 
                    TouchPhase = TouchPhase.Moved
                });
            }
            else if(Input.GetMouseButtonUp(0))
            {
                _signalBus.Fire(new TouchSignal
                {
                    Position = Input.mousePosition, 
                    TouchPhase = TouchPhase.Ended
                });
            }
            
        }
    }
}


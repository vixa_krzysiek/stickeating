namespace Vixa
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class PlayerPrefsStorage : IStorage
    {
        private const string SAVE_DATA_SAVE_KEY = "save_data";

        private SaveData _saveData;
        private Dictionary<string, object> _save;

        public void Initialize()
        {
            _saveData = new SaveData();
            _saveData.Keys = new List<string>();
            _saveData.Types = new List<SavingType>();
            _save = new Dictionary<string, object>();

            var savedJson = PlayerPrefs.GetString(SAVE_DATA_SAVE_KEY);
            if (!string.IsNullOrEmpty(savedJson))
            {
                _saveData = JsonUtility.FromJson<SaveData>(savedJson);
                for (var i = 0; i < _saveData.Keys.Count; i++)
                {
                    switch (_saveData.Types[i])
                    {
                        case SavingType.Boolean:
                            _save.Add(_saveData.Keys[i], PlayerPrefs.GetInt(_saveData.Keys[i]) > 0);
                            break;
                        case SavingType.Float:
                            _save.Add(_saveData.Keys[i], PlayerPrefs.GetFloat(_saveData.Keys[i]));
                            break;
                        case SavingType.Integer:
                            _save.Add(_saveData.Keys[i], PlayerPrefs.GetInt(_saveData.Keys[i]));
                            break;
                        case SavingType.String:
                            _save.Add(_saveData.Keys[i], PlayerPrefs.GetString(_saveData.Keys[i]));
                            break;
                    }
                }
            }
        }

        public void Save(string id, int value)
        {
            if (_save.ContainsKey(id))
            {
                _save[id] = value;
            }
            else
            {
                _save.Add(id, value);
                _saveData.Keys.Add(id);
                _saveData.Types.Add(SavingType.Integer);
            }

            PlayerPrefs.SetInt(id, value);
            SaveSavingData();
        }

        public void Save(string id, float value)
        {
            if (_save.ContainsKey(id))
            {
                _save[id] = value;
            }
            else
            {
                _save.Add(id, value);
                _saveData.Keys.Add(id);
                _saveData.Types.Add(SavingType.Float);
            }

            PlayerPrefs.SetFloat(id, value);
            SaveSavingData();
        }

        public void Save(string id, string value)
        {
            if (_save.ContainsKey(id))
            {
                _save[id] = value;
            }
            else
            {
                _save.Add(id, value);
                _saveData.Keys.Add(id);
                _saveData.Types.Add(SavingType.String);
            }

            PlayerPrefs.SetString(id, value);
            SaveSavingData();
        }

        public void Save(string id, bool value)
        {
            if (_save.ContainsKey(id))
            {
                _save[id] = value;
            }
            else
            {
                _save.Add(id, value);
                _saveData.Keys.Add(id);
                _saveData.Types.Add(SavingType.Boolean);
            }

            PlayerPrefs.SetInt(id, value ? 1 : 0);
            SaveSavingData();
        }

        public int LoadInt(string id, int defaultValue)
        {
            if (_save.ContainsKey(id))
            {
                return (int) _save[id];
            }

            return defaultValue;
        }

        public float LoadFloat(string id, float defaultValue)
        {
            if (_save.ContainsKey(id))
            {
                return (float) _save[id];
            }

            return defaultValue;
        }

        public string LoadString(string id, string defaultValue)
        {
            if (_save.ContainsKey(id))
            {
                return (string) _save[id];
            }

            return defaultValue;
        }

        public bool LoadBool(string id, bool defaultValue)
        {
            if (_save.ContainsKey(id))
            {
                return (bool) _save[id];
            }

            return defaultValue;
        }

        private void SaveSavingData()
        {
            var json = JsonUtility.ToJson(_saveData);
            PlayerPrefs.SetString(SAVE_DATA_SAVE_KEY, json);
            PlayerPrefs.Save();
        }

        public enum SavingType
        {
            Integer = 0,
            Float = 1,
            String = 2,
            Boolean = 3
        }

        [Serializable]
        public class SaveData
        {
            public List<string> Keys;
            public List<SavingType> Types;
        }
    }
}
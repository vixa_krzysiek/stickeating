namespace Vixa
{
    using Zenject;

    public class StorageInstaller : Installer<StorageInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<PlayerPrefsStorage>().AsSingle().NonLazy();
        }
    }
}
﻿namespace Vixa
{
    using Zenject;

    public interface IStorage : IInitializable
    {
        void Save(string id, int value);
        void Save(string id, float value);
        void Save(string id, string value);
        void Save(string id, bool value);

        int LoadInt(string id, int defaultValue = 0);
        float LoadFloat(string id, float defaultValue = 0);
        string LoadString(string id, string defaultValue = "");
        bool LoadBool(string id, bool defaultValue = false);
    }
}


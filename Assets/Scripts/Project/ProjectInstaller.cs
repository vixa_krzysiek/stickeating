namespace Vixa
{
    using UnityEngine;
    using Zenject;

    public class ProjectInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Application.targetFrameRate = 60;
            
            SignalBusInstaller.Install(Container);
            InputInstaller.Install(Container);
            StorageInstaller.Install(Container);
        }
    }
}
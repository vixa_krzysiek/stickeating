﻿namespace Vixa
{
	using UnityEngine;

	public class FpsDisplay : MonoBehaviour
	{
		[SerializeField] private Color _displayColor = new Color(0.0f, 1.0f, 0.5f, 1.0f);
		
		private float _deltaTime;
		
		private void Update()
		{
			_deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
		}

		private void OnGUI()
		{
			var w = Screen.width; 
			var h = Screen.height;

			var style = new GUIStyle();

			var rect = new Rect(0, 0, w, h * 2f / 100);
			style.alignment = TextAnchor.UpperLeft;
			style.fontSize = h * 2 / 100;
			style.normal.textColor = _displayColor;
			var msec = _deltaTime * 1000.0f;
			var fps = 1.0f / _deltaTime;
			var text = $"v{Application.version}, {msec:0.0} ms ({fps:0.} fps)";
			GUI.Label(rect, text, style);
		}
	}

}
